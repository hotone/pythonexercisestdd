class RomanNumeralsConverter:

    special_numerals = [
        (1000, "M"),
        (900, "CM"),
        (100, "C"),
        (90, "XC"),
        (50, "L"),
        (40, "XL"),
        (10, "X"),
        (9, "IX"),
        (5, "V"),
        (4, "IV"),
        (1, "I")
    ]

    @staticmethod
    def convert_to_string(arabic_number):
        roman_number = ""
        for number, symbol in RomanNumeralsConverter.special_numerals:
            while arabic_number >= number:
                roman_number += symbol
                arabic_number -= number
        return roman_number

