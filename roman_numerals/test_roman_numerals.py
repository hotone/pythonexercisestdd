from roman_numerals.roman_numerals import RomanNumeralsConverter
import pytest


@pytest.mark.parametrize(
    "arabic, roman",
    [
        pytest.param(1, "I"),
        pytest.param(2, "II"),
        pytest.param(3, "III"),
        pytest.param(4, "IV"),
        pytest.param(5, "V"),
        pytest.param(6, "VI"),
        pytest.param(7, "VII"),
        pytest.param(8, "VIII"),
        pytest.param(9, "IX"),
        pytest.param(10, "X"),
        pytest.param(50, "L"),
        pytest.param(100, "C"),
        pytest.param(1000, "M"),
    ]
)
def test_convert_arabic_to_roman_numeral_string(arabic, roman):
    assert RomanNumeralsConverter.convert_to_string(arabic) == roman


def test_acceptance():
    assert RomanNumeralsConverter.convert_to_string(1234) == "MCCXXXIV"
    assert RomanNumeralsConverter.convert_to_string(1999) == "MCMXCIX"



