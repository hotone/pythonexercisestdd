import pytest
from prime_numbers_factorization.factorization import factorize_number


@pytest.mark.parametrize(
    "number, expected_factors",
    [
        pytest.param(1, []),
        pytest.param(2, [2]),
        pytest.param(3, [3]),
        pytest.param(4, [2, 2]),
        pytest.param(5, [5]),
        pytest.param(6, [2, 3]),
        pytest.param(7, [7]),
        pytest.param(8, [2, 2, 2]),
        pytest.param(9, [3, 3]),
        pytest.param(10, [2, 5]),
        pytest.param(11, [11]),
        pytest.param(12, [2, 2, 3]),
        pytest.param(13, [13]),
        pytest.param(2*2*2*3*3*5*7*11*13, [2, 2, 2, 3, 3, 5, 7, 11, 13]),
        pytest.param(11*11*13*13*17*17*23*23*67, [11, 11, 13, 13, 17, 17, 23, 23, 67]),
        pytest.param(2**10, [2 for x in range(10)]),
        pytest.param(2**100, [2 for x in range(100)]),
        pytest.param(2**1000, [2 for x in range(1000)]),
    ]
)
def test_factorize_number_and_expect_array(number, expected_factors):
    assert factorize_number(number) == expected_factors

