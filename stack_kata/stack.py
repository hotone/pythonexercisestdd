
class Stack(object):

    def __init__(self):
        self.__elements = []
        self.__size = 0

    def is_empty(self):
        if len(self.__elements) != 0:
            return False
        else:
            return True

    def push(self, element):
        self.__elements.append(element)
        self.__size += 1

    def pop(self):
        if self.__size == 0:
            raise Stack.UnderflowError("Cannot pop empty stack_kata!")
        else:
            self.__size -= 1
            return self.__elements.pop()

    def get_size(self):
        return self.__size

    class UnderflowError(Exception):
        pass

