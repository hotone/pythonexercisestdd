import pytest
from stack_kata.stack import Stack


class TestStack:

    @pytest.fixture
    def stack(self):
        stack = Stack()
        return stack

    def test_is_new_stack_empty(self, stack):
        assert stack.is_empty()

    def test_is_pushed_stack_not_empty(self, stack):
        stack.push(1)
        assert not stack.is_empty()

    def test_push_and_pop(self, stack):
        stack.push(1)
        assert stack.pop() == 1

    def test_push_twice_and_pop(self, stack):
        stack.push(88)
        stack.push(99)
        assert stack.pop() == 99

    def test_push_and_check_size_is_one(self, stack):
        stack.push(88)
        assert stack.get_size() == 1

    def test_push_twice_and_check_size_is_two(self, stack):
        stack.push(99)
        stack.push(11)
        assert stack.get_size() == 2

    def test_try_pop_empty_stack(self, stack):
        with pytest.raises(Stack.UnderflowError):
            assert stack.get_size() == 0
            stack.pop()

    def test_push_twice_and_pop_once(self,stack):
        stack.push(99)
        stack.push(100)
        assert stack.pop() == 100

